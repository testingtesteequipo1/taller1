
from math import *
from tkinter.ttk import *
from tkinter import *


cal = Tk()
cal.title("Calculadora Testing")

txtDisplay = Entry(cal, font=('arial', 20, 'bold'), bd=30, insertwidth=4, bg="powder blue", justify='right')

class Calculator:

    def __init__(self):
        self.var1 = ""
        self.var2 = ""
        self.output = 0
        self.current = 0
        self.operator = ""
        self.isButton = 0
        self.isRightParen = 0
        self.aux=0
        self.operacion=""

    def btnClick(self, num):
        if self.isRightParen is 1:
            self.operator = self.operator + "*" + str(num)
            txtDisplay.delete(0, END)
            txtDisplay.insert(0, self.operator)
            self.expOutput(self.operator)
            self.isButton = 1
            self.isRightParen = 0

        elif self.isRightParen is 0:
            self.operator = self.operator + str(num)
            txtDisplay.delete(0, END)
            txtDisplay.insert(0, self.operator)
            self.expOutput(self.operator)
            self.isButton = 1
            self.isRightParen = 0

    def leftParenthesesClick(self):

        if self.isRightParen is 1:
            self.operator = self.operator + "*" + "("
            txtDisplay.delete(0, END)
            txtDisplay.insert(0, self.operator)
            self.isRightParen = 0

        elif self.isButton is 0:
            self.operator = self.operator + "("
            txtDisplay.delete(0, END)
            txtDisplay.insert(0, self.operator)
            self.isRightParen = 0
        elif self.isButton is 1:
            self.operator = self.operator + "*" + "("
            txtDisplay.delete(0, END)
            txtDisplay.insert(0, self.operator)
            self.isRightParen = 0

    def rightParenthesesClick(self):
        self.operator = self.operator + ")"
        txtDisplay.delete(0, END)
        txtDisplay.insert(0, self.operator)
        self.expOutput(self.operator)
        self.isRightParen = 1
        self.aux=1

    def porc(self):
        self.operacion = "porc"
        self.operator = self.operator + "%"
        self.isButton = 0
        self.isRightParen = 0
        self.expOutput(self.operator)

    def expOutput(self, operator):
        try:
            self.output = str(eval(operator))
            txtDisplay.delete(0, END)
            txtDisplay.insert(0, string=self.operator + "=" + self.output)
            self.current = 0
        except SyntaxError:
                txtDisplay.delete(0, END)
                #txtDisplay.insert(0,"Syntax Error")
                txtDisplay.insert(0, self.operator)


    def oprtrClick(self, op):
        if self.current is 0:
            self.current = 1
            self.operator = self.operator + op
            txtDisplay.delete(0, END)
            txtDisplay.insert(0, string=self.operator)
            self.isButton = 0
            self.isRightParen = 0
        else:
            self.operator = self.operator + op
            self.isButton = 0
            self.isRightParen = 0
            self.expOutput(self.operator)

    # def equals(self):
    #     self.operator = self.result
    #     txtDisplay.delete(0, END)
    #     txtDisplay.insert(0, string=self.operator)

    def clear(self):
        self.__init__()
        txtDisplay.delete(0, END)

    def delete(self):
        self.operator = self.operator[: -1]
        txtDisplay.delete(0, END)
        txtDisplay.insert(0, string=self.operator)

def Selecttype(event):
    global combo2
    global combo3
    combo2 = Combobox(cal)
    combo3 = Combobox(cal)
    if (combo.get()=="Volumen"):
        combo2['values']= ("De" , "cm3", "m3", "litros", "galones")
        combo2.grid(column=6, row=2)
        combo2.current(0)
        combo3['values']= ("A" , "cm3", "m3", "litros", "galones")
        combo3.grid(column=6, row=3)
        combo3.current(0)


    elif (combo.get()=="Longitud"):
        combo2['values']= ("De" , "pulgadas", "mm", "cm", "m", "km")
        combo2.grid(column=6, row=2)
        combo2.current(0)
        combo3['values']= ("A" , "pulgadas", "mm", "cm", "m", "km")
        combo3.grid(column=6, row=3)
        combo3.current(0)


    elif (combo.get()=="Temperatura"):
        combo2['values']= ("De" , "Fahrenheit", "Celsius", "Kelvin")
        combo2.grid(column=6, row=2)
        combo2.current(0)
        combo3['values']= ("A" , "Fahrenheit", "Celsius", "Kelvin")
        combo3.grid(column=6, row=3)
        combo3.current(0)

    elif (combo.get()=="Peso"):
        combo2['values']= ("De" , "gramos", "kilogramos", "toneladas", "libras")
        combo2.grid(column=6, row=2)
        combo2.current(0)
        combo3['values']= ("A" , "gramos", "kilogramos", "toneladas", "libras")
        combo3.grid(column=6, row=3)
        combo3.current(0)

    elif (combo.get()=="Presión"):
        combo2['values']= ("De" , "pascal", "atm", "mmHg")
        combo2.grid(column=6, row=2)
        combo2.current(0)
        combo3['values']= ("A" , "pascal", "atm", "mmHg")
        combo3.grid(column=6, row=3)
        combo3.current(0)
        
def convert():
    #Volumen
    if(combo2.get()=="cm3" and combo3.get()=="m3"):
        a=int(txtDisplay2.get())
        a=a/1000000
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="cm3" and combo3.get()=="litros"):
        a=int(txtDisplay2.get())
        a=a/1000
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="cm3" and combo3.get()=="galones"):
        a=int(txtDisplay2.get())
        a=a/3785.412
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="m3" and combo3.get()=="cm3"):
        a=int(txtDisplay2.get())
        a=a*1000000
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="m3" and combo3.get()=="litros"):
        a=int(txtDisplay2.get())
        a=a*1000
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="m3" and combo3.get()=="galones"):
        a=int(txtDisplay2.get())
        a=a*264.172
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="litros" and combo3.get()=="cm3"):
        a=int(txtDisplay2.get())
        a=a*1000
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="litros" and combo3.get()=="m3"):
        a=int(txtDisplay2.get())
        a=a/1000
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="litros" and combo3.get()=="galones"):
        a=int(txtDisplay2.get())
        a=a/3.785
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="galones" and combo3.get()=="cm3"):
        a=int(txtDisplay2.get())
        a=a*3785.412
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="galones" and combo3.get()=="m3"):
        a=int(txtDisplay2.get())
        a=a/264.172
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="galones" and combo3.get()=="litros"):
        a=int(txtDisplay2.get())
        a=a*3.785
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    #Longitud
    elif (combo2.get()=="pulgadas" and combo3.get()=="mm"):
        a=int(txtDisplay2.get())
        a=a*25.4
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="pulgadas" and combo3.get()=="cm"):
        a=int(txtDisplay2.get())
        a=a*2.54
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="pulgadas" and combo3.get()=="m"):
        a=int(txtDisplay2.get())
        a=a/39.37
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="pulgadas" and combo3.get()=="km"):
        a=int(txtDisplay2.get())
        a=a/39370.079
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="mm" and combo3.get()=="pulgadas"):
        a=int(txtDisplay2.get())
        a=a/25.4
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="mm" and combo3.get()=="cm"):
        a=int(txtDisplay2.get())
        a=a/10
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="mm" and combo3.get()=="m"):
        a=int(txtDisplay2.get())
        a=a/1000
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="mm" and combo3.get()=="km"):
        a=int(txtDisplay2.get())
        a=a/1000000
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="cm" and combo3.get()=="pulgadas"):
        a=int(txtDisplay2.get())
        a=a/2.54
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="cm" and combo3.get()=="mm"):
        a=int(txtDisplay2.get())
        a=a*10
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="cm" and combo3.get()=="m"):
        a=int(txtDisplay2.get())
        a=a/100
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="cm" and combo3.get()=="km"):
        a=int(txtDisplay2.get())
        a=a/100000
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="m" and combo3.get()=="pulgadas"):
        a=int(txtDisplay2.get())
        a=a*39.37
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="m" and combo3.get()=="cm"):
        a=int(txtDisplay2.get())
        a=a*100
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="m" and combo3.get()=="km"):
        a=int(txtDisplay2.get())
        a=a/1000
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="km" and combo3.get()=="pulgadas"):
        a=int(txtDisplay2.get())
        a=a*39370.079
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="km" and combo3.get()=="mm"):
        a=int(txtDisplay2.get())
        a=a*1000000
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="km" and combo3.get()=="cm"):
        a=int(txtDisplay2.get())
        a=a*100000
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="km" and combo3.get()=="m"):
        a=int(txtDisplay2.get())
        a=a*1000
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
#Temperatura
    elif (combo2.get()=="Fahrenheit" and combo3.get()=="Celsius"):
        a=int(txtDisplay2.get())
        a=(a-32)*5/9
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="Fahrenheit" and combo3.get()=="Kelvin"):
        a=int(txtDisplay2.get())
        a=(a-32) * 5/9 + 273.15
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="Celsius" and combo3.get()=="Fahrenheit"):
        a=int(txtDisplay2.get())
        a=(a*9/5) + 32
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="Celsius" and combo3.get()=="Kelvin"):
        a=int(txtDisplay2.get())
        a=a + 273.15
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="Kelvin" and combo3.get()=="Fahrenheit"):
        a=int(txtDisplay2.get())
        a=(a - 273.15) * 9/5 + 32
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="Kelvin" and combo3.get()=="Celsius"):
        a=int(txtDisplay2.get())
        a=a - 273.15
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
#Peso
    elif (combo2.get()=="gramos" and combo3.get()=="kilogramos"):
        a=int(txtDisplay2.get())
        a=a/1000
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="gramos" and combo3.get()=="toneladas"):
        a=int(txtDisplay2.get())
        a=a/1000000
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="gramos" and combo3.get()=="libras"):
        a=int(txtDisplay2.get())
        a=a/453.592
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="kilogramos" and combo3.get()=="gramos"):
        a=int(txtDisplay2.get())
        a=a*1000
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="kilogramos" and combo3.get()=="toneladas"):
        a=int(txtDisplay2.get())
        a=a/1000
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="kilogramos" and combo3.get()=="libras"):
        a=int(txtDisplay2.get())
        a=a*2.205
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="toneladas" and combo3.get()=="gramos"):
        a=int(txtDisplay2.get())
        a=a*1000000
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="toneladas" and combo3.get()=="kilometros"):
        a=int(txtDisplay2.get())
        a=a*1000
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="toneladas" and combo3.get()=="libras"):
        a=int(txtDisplay2.get())
        a=a*2204.623
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="libras" and combo3.get()=="gramos"):
        a=int(txtDisplay2.get())
        a=a*453.592
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="libras" and combo3.get()=="kilogramos"):
        a=int(txtDisplay2.get())
        a=a/2.205
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="libras" and combo3.get()=="toneladas"):
        a=int(txtDisplay2.get())
        a=a/2204.623
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
#Presion
    elif (combo2.get()=="pascal" and combo3.get()=="atm"):
        a=int(txtDisplay2.get())
        a=a/101325
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="pascal" and combo3.get()=="mmHg"):
        a=int(txtDisplay2.get())
        a=a/133.322
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="atm" and combo3.get()=="pascal"):
        a=int(txtDisplay2.get())
        a=a*101325
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="atm" and combo3.get()=="mmHg"):
        a=int(txtDisplay2.get())
        a=a/760
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="mmHg" and combo3.get()=="pascal"):
        a=int(txtDisplay2.get())
        a=a*133.322
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    elif (combo2.get()=="mmHg" and combo3.get()=="atm"):
        a=int(txtDisplay2.get())
        a=a*760
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)
    else:
        a=int(txtDisplay2.get())
        txtDisplay3.delete(0, END)
        txtDisplay3.insert(0,a)

smartCal = Calculator()

btn0 = Button(cal, padx=16, pady=16, bd=8, fg="black", font=('arial', 20, 'bold'), text="0",bg="powder blue", command=lambda: smartCal.btnClick(0))

btn1 = Button(cal, padx=16, pady=16, bd=8, fg="black", font=('arial', 20, 'bold'), text="1", bg="powder blue", command=lambda: smartCal.btnClick(1))

btn2 = Button(cal, padx=16, pady=16, bd=8, fg="black", font=('arial', 20, 'bold'), text="2", bg="powder blue", command=lambda: smartCal.btnClick(2))

btn3 = Button(cal, padx=16, pady=16, bd=8, fg="black", font=('arial', 20, 'bold'), text="3", bg="powder blue", command=lambda: smartCal.btnClick(3))

btn4 = Button(cal, padx=16, pady=16, bd=8, fg="black", font=('arial', 20, 'bold'), text="4", bg="powder blue", command=lambda: smartCal.btnClick(4))

btn5 = Button(cal, padx=16, pady=16, bd=8, fg="black", font=('arial', 20, 'bold'), text="5", bg="powder blue", command=lambda: smartCal.btnClick(5))

btn6 = Button(cal, padx=16, pady=16, bd=8, fg="black", font=('arial', 20, 'bold'), text="6", bg="powder blue", command=lambda: smartCal.btnClick(6))

btn7 = Button(cal, padx=16, pady=16, bd=8, fg="black", font=('arial', 20, 'bold'), text="7", bg="powder blue", command=lambda: smartCal.btnClick(7))

btn8 = Button(cal, padx=16, pady=16, bd=8, fg="black", font=('arial', 20, 'bold'), text="8", bg="powder blue", command=lambda: smartCal.btnClick(8))

btn9 = Button(cal, padx=16, pady=16, bd=8, fg="black", font=('arial', 20, 'bold'), text="9", bg="powder blue", command=lambda: smartCal.btnClick(9))

btnDecimal = Button(cal, padx=16, pady=16, bd=8, fg="black", font=('arial', 20, 'bold'), text=".", bg="powder blue", command=lambda: smartCal.btnClick("."))

btnLeftParen = Button(cal, padx=16, pady=16, bd=8, fg="black", font=('arial', 20, 'bold'), text="(", bg="powder blue", command=lambda: smartCal.btnClick("("))

btnRightParen = Button(cal, padx=16, pady=16, bd=8, fg="black", font=('arial', 20, 'bold'), text=")", bg="powder blue", command=lambda: smartCal.btnClick(")"))

Add_btn = Button(cal, padx=16, pady=16, bd=8, fg="black", font=('arial', 20, 'bold'), text="+", bg="powder blue", command=lambda: smartCal.oprtrClick("+"))

Sub_btn = Button(cal, padx=16, pady=16, bd=8, fg="black", font=('arial', 20, 'bold'), text="-", bg="powder blue", command=lambda: smartCal.oprtrClick("-"))

Mul_btn = Button(cal, padx=16, pady=16, bd=8, fg="black", font=('arial', 20, 'bold'), text="*", bg="powder blue", command=lambda: smartCal.oprtrClick("*"))

Div_btn = Button(cal, padx=16, pady=16, bd=8, fg="black", font=('arial', 20, 'bold'), text="/", bg="powder blue", command=lambda: smartCal.oprtrClick("/"))

Pow_btn = Button(cal, padx=16, pady=16, bd=8, fg="black", font=('arial', 20, 'bold'), text="^", bg="powder blue", command=lambda: smartCal.oprtrClick("**"))

Sqrt_btn = Button(cal, padx=16, pady=16, bd=8, fg="black", font=('arial', 20, 'bold'), text="√", bg="powder blue", command=lambda: smartCal.oprtrClick("sqrt("))

Log_btn = Button(cal, padx=16, pady=16, bd=8, fg="black", font=('arial', 20, 'bold'), text="log", bg="powder blue", command=lambda: smartCal.oprtrClick("log("))

Porc_btn = Button(cal, padx=16, pady=16, bd=8, fg="black", font=('arial', 20, 'bold'), text="%", bg="powder blue", command=lambda: smartCal.oprtrClick("*100/"))

Rene_btn = Button(cal, padx=16, pady=16, bd=8, fg="black", font=('arial', 20, 'bold'), text="n√", bg="powder blue", command=lambda: smartCal.oprtrClick("**(1/"))

Fracc_btn = Button(cal, padx=16, pady=16, bd=8, fg="black", font=('arial', 20, 'bold'), text="1/2", bg="powder blue", command=lambda: smartCal.oprtrClick(""))

Pi_btn = Button(cal, padx=16, pady=16, bd=8, fg="black", font=('arial', 20, 'bold'), text="π", bg="powder blue", command=lambda: smartCal.oprtrClick("pi"))

Euler_btn = Button(cal, padx=16, pady=16, bd=8, fg="black", font=('arial', 20, 'bold'), text="e", bg="powder blue", command=lambda: smartCal.oprtrClick("e"))

btnClear = Button(cal, padx=16, pady=16, bd=8, fg="black", font=('arial', 20, 'bold'), text="C", bg="powder blue", command=smartCal.clear)

btnBackspace = Button(cal, padx=16, pady=16, bd=8, fg="black", font=('arial', 20, 'bold'), text="⌫", bg="powder blue", command=smartCal.delete)

Convert_btn = Button(cal, padx=16, pady=16, bd=8, fg="black", font=('arial', 15, 'bold'), text="Convertir", bg="powder blue", command=convert)

# ~*~*~Positioning~*~*~

txtDisplay.grid(columnspan=6)
# =========ROW1================== #
btn7.grid(row=1, column=0)
btn8.grid(row=1, column=1)
btn9.grid(row=1, column=2)
Add_btn.grid(row=1, column=3)
Pi_btn.grid(row=1, column=4)
# =========ROW2================== #
btn4.grid(row=2, column=0)
btn5.grid(row=2, column=1)
btn6.grid(row=2, column=2)
Sub_btn.grid(row=2, column=3)
Rene_btn.grid(row=2, column=4)
# =========ROW3================== #
btn1.grid(row=3, column=0)
btn2.grid(row=3, column=1)
btn3.grid(row=3, column=2)
Mul_btn.grid(row=3, column=3)
Sqrt_btn.grid(row=3, column=4)
# =========ROW4================== #
btn0.grid(row=4, column=0)
btnLeftParen.grid(row=4, column=1)
btnRightParen.grid(row=4, column=2)
# btnEquals.grid(row=4, column=2)
Div_btn.grid(row=4, column=3)
Porc_btn.grid(row=4, column=4)
# =========ROW5================== #
btnDecimal.grid(row=5, column=0)
Euler_btn.grid(row=5, column=1)
Pow_btn.grid(row=5, column=2)
Log_btn.grid(row=5, column=3)
btnClear.grid(row=5, column=4)
btnBackspace.grid(row=5, column=5)
Convert_btn.grid(row=5, column=6)


txtDisplay2 = Entry(cal, font=('arial', 20, 'bold'), bd=30, insertwidth=10, bg="powder blue", justify='right')
txtDisplay2.grid(row=1,column=6)
txtDisplay3 = Entry(cal, font=('arial', 20, 'bold'), bd=30, insertwidth=10, bg="powder blue", justify='right')
txtDisplay3.grid(row=4,column=6)

combo = Combobox(cal)

combo['values']= ("Ingrese una opción", "Volumen", "Longitud", "Temperatura", "Peso", "Presión")

combo.current(0)

combo.grid(column=6, row=0)
combo.bind("<<ComboboxSelected>>", Selecttype)

cal.mainloop()

