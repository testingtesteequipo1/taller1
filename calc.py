# -*- coding: utf-8 -*- 
import math

class Operaciones:
    def __init__ (self,x,y=0):
      self.x=x
      self.y=y

    def sumar(self):
        return (str(int(self.x+self.y)))
        #return self.x+self

    def restar (self):
        
        return (str(int(self.x-self.y)))

    def mult(self):
        return (str(int(self.x*self.y)))
  
    def div(self):
        if (self.y==0):
            return ("Inserte un numero distinto de 0")
        else:
            if (self.x==0):
                return ("0")
            else:
                if (self.y==1):
                  return (str(int(self.x)))
                else:
                    return (str(self.x/self.y))

    def potencia (self):
        return(str(pow(self.x,self.y)))
    
    def porcentaje(self):
        if (self.y == 0):
            return ("Inserte un numero distinto de 0")
        else:
            return (str((self.x/self.y)*100))

    def raiz (self):
        if (self.x >= 0):
            return(str(math.sqrt (self.x)))
        else :
            return ("Inserte un numero positivo")

    def log (self):
        if (self.x > 0):
            logaritmo=math.log10(self.x)
            return(str(logaritmo))
        else:
            return("Inserte un numero mayor a 0")
        
    def re (self):
        print(str(pow(self.x,(1/self.y))))
    
