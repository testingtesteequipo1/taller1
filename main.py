# -*- coding: utf-8 -*-
import calc
from calc import *
#def menu():
x=1
#taller= Operaciones()

while(x!=0):
    print ("Selecciona una opcion")
    print ("1.-Sumar")
    print ("2.-Restar")
    print ("3.-Multiplicar")
    print ("4.-Division")
    print ("5.-Potencia")
    print ("6.-Porcentaje")
    print ("7.-Raiz cuadrada")
    print ("8.-Logaritmo")
    print ("9.-Renesima")
    print ("0.-Salir")
    x = int(input("\nSeleciona la operación que desees: "))
    #x = menu()

    #if (x==0):
    #    break
    if (x==1):
        print ("\nSeleccionaste suma\n ")
        num1 = int(input("\nIngrese el primer numero: "))
        num2 = int(input("\nIngrese el segundo numero: "))
        y = calc.Operaciones(num1,num2)
        y.sumar()

    elif(x==2):
        print ("\nSeleccionaste resta \n")
        num1 = int(input("\nIngrese el primer numero: "))
        num2 = int(input("\nIngrese el segundo numero: "))
        y = calc.Operaciones(num1,num2)
        y.restar()

    elif(x==3):
        print ("\nSeleccionaste multiplicación\n ")
        num1 = int(input("\nIngrese el primer numero: "))
        num2 = int(input("\nIngrese el segundo numero: "))
        y = calc.Operaciones(num1,num2)
        y.mult()

    elif (x==4):
        print ("\nSeleccionaste división \n")
        num1 = int(input("\nIngrese el primer numero: "))
        num2 = int(input("\nIngrese el segundo numero: "))
        y = calc.Operaciones(num1,num2)
        y.div()

    elif (x==5):        
        print ("\nSeleccionaste potencia \n")
        num1 = int(input("\nIngrese el primer numero: "))
        num2 = int(input("\nIngrese el segundo numero: "))
        y = calc.Operaciones(num1,num2)
        y.potencia()

    elif (x==6):
        print ("\nSeleccionaste porcentaje \n")
        num1 = int(input("\nIngrese el primer numero: "))
        num2 = int(input("\nIngrese el segundo numero: "))
        y = calc.Operaciones(num1,num2)
        y.porcentaje()

    elif (x==7):
        print ("\nSeleccionaste raíz cuadrada\n")
        num1 = int(input("Ingrese un numero para calcular su raiz cuadrada: "))
        y= calc.Operaciones(num1)
        y.raiz()

    elif (x==8):
        print ("\nSeleccionaste logaritmo \n")
        num1 = int(input("Ingrese un numero para calcular su logaritmo en base 10: "))
        y= calc.Operaciones(num1)
        y.log()

    elif (x==9):
        print ("\nSeleccionaste raíz enesima\n")
        num1 = int(input("Ingrese un numero para calcular su raiz : "))
        num2 = int(input("Ingrese numero para calcular raiz :  "))
        y= calc.Operaciones(num1,num2)
        y.re()
        
    elif (x==0):
        print ("\n\n\nGracias por usar la calculadora\n\n\n")
        exit

    else:
        print ("\n\n\nOpción no valida\n\n\n")
